package com.princess.sms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.princess.sms.model.Language;
import com.princess.sms.model.LanguageList;
import com.princess.sms.service.LanguageService;

@RunWith(PowerMockRunner.class)
public class LanguageServiceImplTest {

	@InjectMocks
	private LanguageService languageService = new LanguageServiceImpl();

	@Mock
	RestTemplate restTemplate;

	@Mock
	String shipstarWSURL;

	@Before
	public void setUp() {
		Whitebox.setInternalState(languageService, String.class, shipstarWSURL);
	}
	
	@Test
	  public void testFindLanguageList() {
	    StringBuilder wsUrl = new StringBuilder(shipstarWSURL).append("languages/supported");

	    List<Language>languages = new ArrayList<>();
	    Language language = new Language();
	    language.setCountryCode("US");
	    language.setLanguageCode("EN");
	    language.setDescription("English");
	    language.setSupported(true);

	    Language language1 = new Language();
	    language.setCountryCode("JP");
	    language.setLanguageCode("JA");
	    language.setDescription("JAPANESE");
	    language.setSupported(true);

	    Language language2 = new Language();
	    language.setCountryCode("CN");
	    language.setLanguageCode("ZH");
	    language.setDescription("CHINESE");
	    language.setSupported(true);
	    
	    languages.add(language);
	    languages.add(language1);
	    languages.add(language2);
	    
	    LanguageList expectedLanguageList = new LanguageList(languages);
	    
	    ResponseEntity<LanguageList> response = new ResponseEntity<LanguageList>(expectedLanguageList, HttpStatus.ACCEPTED);
	    Mockito.when(restTemplate.getForEntity(wsUrl.toString(), LanguageList.class)).thenReturn(response);

	    LanguageList actualLanguageList = languageService.findSupportedLanguages();

	    Assert.assertEquals(expectedLanguageList, actualLanguageList);
	  }

}
