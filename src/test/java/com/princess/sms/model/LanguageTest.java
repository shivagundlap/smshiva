package com.princess.sms.model;

public class LanguageTest extends AbstractJavaBeanTest<Language>{

	@Override
	protected Language getBeanInstance() {
		return new Language();
	}

}
