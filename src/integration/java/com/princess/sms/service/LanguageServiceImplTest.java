package com.princess.sms.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.princess.sms.model.LanguageList;

public class LanguageServiceImplTest extends AbstractServiceIntegrationTestCase{

	@Autowired
	private LanguageService languageService;
	
	@Test
	 public void testFindLanguageList() {
		LanguageList list = languageService.findSupportedLanguages();
		Assert.assertEquals(3, list.getLanguages().size());
	}
}
